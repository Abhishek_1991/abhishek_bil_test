'use strict';
/**
 * @ngdoc overview
 * @name bilApp
 * @description
 * # ngApp
 * 
 * v0.0.1   
 *
 * Main module of the application.
 */
angular.module('ngApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
]).config(["$routeProvider", function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'screens/1_1.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/1_2', {
            templateUrl: 'screens/1_2.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })		
        .when('/2_1', {
            templateUrl: 'screens/2_1.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/2_2', {
            templateUrl: 'screens/2_2.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/2_3', {
            templateUrl: 'screens/2_3.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/2_4', {
            templateUrl: 'screens/2_4.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/2_5', {
            templateUrl: 'screens/2_5.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/2_6', {
            templateUrl: 'screens/2_6.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/2_7', {
            templateUrl: 'screens/2_7.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })		
        .when('/3', {
            templateUrl: 'screens/3.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/4', {
            templateUrl: 'screens/4.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/5_1', {
            templateUrl: 'screens/5_1.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/5_2', {
            templateUrl: 'screens/5_2.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })	
        .when('/5_3', {
            templateUrl: 'screens/5_3.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })	
        .when('/5_4', {
            templateUrl: 'screens/5_4.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })	
        .when('/5_5', {
            templateUrl: 'screens/5_5.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })	
        .when('/5_6', {
            templateUrl: 'screens/5_6.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })	
        .when('/5_7', {
            templateUrl: 'screens/5_7.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })			
        .when('/6', {
            templateUrl: 'screens/6.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/tst', {
            templateUrl: 'views/tst.html',
            controllerAs: 'main2'
        })
        .otherwise({
            redirectTo: '/'
        });
}]);
